<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'OfferController@index');


Auth::routes();
Route::get('users/suspended', 'UserController@suspended')->name('suspended');
Route::resource('users', 'UserController');
Route::any('users/reactive/{id}', 'UserController@reactive')->name('reactive');
Route::get('/home', 'OfferController@index');
Route::get('/offer', 'OfferController@index')->name('index');
Route::get('/offer/create', 'OfferController@create')->name('create');
Route::post('/offer', 'OfferController@post')->name('offer');
Route::get('/offer/{one}', 'OfferController@view');
Route::get('/log/', 'NotifyController@Log')->name('log');


Route::get('/notifications/', function () {
    // get the last 100 notifications and return it to json
    $return = [];
    $notiy = \App\Notify::take(100)->orderBy('created_at', 'desc')->get();

    foreach ($notiy as $row) {
        $offer = DB::table('offers')->where('id', $row->offer_id)->first();
        $return[$row->id]['notification_id'] = $row->id;
        $return[$row->id]['text'] = $offer->text;
        $return[$row->id]['link'] = $offer->link;
        $return[$row->id]['photo'] = $_SERVER['HTTP_HOST'] . '/photo/' . $offer->photo;
        $return[$row->id]['date'] = $row->created_at->format('Y-m-d H:i:s');
    }
    return response()->json($return);
});
Route::get('/notifications/{id}', function ($id) {
    // get the last 100 notifications and return it to json
    $return = [];
    $notiy = \App\Notify::where('id', '>', $id)->orderBy('id')->get();

    foreach ($notiy as $row) {
        $offer = DB::table('offers')->where('id', $row->offer_id)->first();
        $return[$row->id]['notification_id'] = $row->id;
        $return[$row->id]['text'] = $offer->text;
        $return[$row->id]['link'] = $offer->link;
        $return[$row->id]['photo'] = $_SERVER['HTTP_HOST'] . '/photo/' . $offer->photo;
        $return[$row->id]['date'] = $row->created_at->format('Y-m-d H:i:s');
    }
    return response()->json($return);
});
// this is the route for  resend notification
Route::post('/resend/{one}', function ($one) {
    if (\App\Notify::Notify($one)) {

        return redirect('offer')->with('status', 'Resend notification  Created!');
    }
    return redirect('offer')->with('status', 'Resend notification  Error!');
})->name('resend')
    ->middleware(['auth', 'admin']);

Route::get('/api/ios', function (Illuminate\Http\Request $request) {
    $token = $request->input('token');
    $user = $request->input('user');

    if (!is_string($token)) {
        return response()->json(['error' => sprintf(
            'Device token must be a string, "%s" given.',
            gettype($token)
        )]);
    }

    if (preg_match('/[^0-9a-fA-F]/', $token)) {
        return response()->json(['error' => sprintf(
            'Device token must be mask "%s". Token given: "%s"',
            '/[^0-9a-f]/',
            $token
        )]);
    }

    if (strlen($token) != 64) {
        return response()->json(['error' => sprintf(
            'Device token must be a 64 charsets, Token length given: %d.',
            mb_strlen($token)
        )]);
    }
    // save the token to the server
    $new_ios = App\IOS::firstOrCreate(['token' => $token,
        'user' => $user,
        'status' => '1']);
    if($new_ios->save()) {
        return response()->json(['success' => 'true']);
    }
    return response()->json(['error' => 'Faild to save Token']);

});

Route::get('/api/android', function (Illuminate\Http\Request $request) {
    $token = $request->input('token');
    $user = $request->input('user');
    if(!$token or !$user){
        return response()->json(['error' => sprintf(
            'Device token and user must be a NOT EMPTY, "%s" given.',
            gettype($token)
        )]);
    }

    // save the token to the server
    $new_ios = App\Android::firstOrCreate([
        'token' => $token,
        'user' => $user,
        'status' => '1']);
 IF ($new_ios->save()) {
      return response()->json(['success' => 'true']);
    }
    return response()->json(['error' => 'Faild to save Token']);

});

// this link will fire the send notify command
Route::get('/send/m',function (){
    //
    $affected = DB::table('notify')->where('status', '=', 1)->update(array('status' => 0));
    Artisan::call('send:notify');
});




