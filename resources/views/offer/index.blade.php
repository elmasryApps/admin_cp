@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Latest Offers</div>

                    <div class="panel-body">
                        @if(count($offers)>0)
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>URL</th>
                                    <th>Photo</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($offers as $offer)
                                    <tr>
                                        <td>
                                            {{$offer->id}}
                                        </td>
                                        <td>
                                            <a href="{{$offer->link}}" target="_blank"> Offer Link</a>
                                        </td>
                                        <td>

                                            <img src="{{url('/')}}/photo/{{$offer->photo}}" class="img-rounded"
                                                 alt="Cinque Terre" width="304" height="236">
                                        </td>
                                        <td>

                                            <a href="{{route('resend',$offer->id)}}"><span class="label label-success">Resend</span></a>
                                            <a href="offer/{{$offer->id}}"><span
                                                        class="label label-info">View </span></a>

                                            <p> The offer logs: <br/>
                                                @foreach($offer->logs() as $logs)
                                                    {{$logs->created_at}} <br/>
                                                @endforeach
                                            </p>

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <p>Create your first <a href="{{Route('create')}}">offer</a></p>
                        @endif
                        {!! $offers->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
