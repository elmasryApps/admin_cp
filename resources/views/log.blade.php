@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Notifications Logs</div>

                    <div class="panel-body">
                        @if(count($log)>0)
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Offer</th>
                                    <th>created_at</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($log as $offer)
                                    <tr>
                                        <td>
                                            {{$offer->id}}
                                        </td>
                                        <td>
                                            <p> {{\App\Notify::offer_text($offer->offer_id)->text}}</p>
                                        </td>
                                        <td>

                                        <p>{{$offer->created_at}}</p>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <p>Your notifications logs is empty Create your first <a href="{{Route('create')}}">offer</a> </p>
                        @endif
                        {!! $log->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
