@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Users</div>

                    <div class="panel-body">
                        @if(count($users)>0)
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>E-mail</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            {{$user->name}}
                                        </td>
                                        <td>
                                            {{$user->email}}
                                        </td>
                                        <td>

                                            <a href="{{route('users.edit',[$user->id])}}"><span
                                                        class="label label-success">Edit</span></a>
                                            @if($user->email!=env('ADMIN_MAIL','NOTHING'))
                                                {{ Form::open(array('url' => 'users/' . $user->id, 'class' => 'pull-right')) }}
                                                {{ Form::hidden('_method', 'DELETE') }}
                                                {{ Form::submit('Suspend', array('class' => 'btn btn-danger')) }}
                                                {{ Form::close() }}
                                            @endif()

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <p>Create your first <a href="{{Route('create')}}">offer</a></p>
                        @endif
                        {!! $users->render() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Suspended Users :</div>

                    <div class="panel-body">
                        @if(count($suspend)>0)
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>E-mail</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($suspend as $user)
                                    <tr>
                                        <td>
                                            {{$user->name}}
                                        </td>
                                        <td>
                                            {{$user->email}}
                                        </td>
                                        <td>
                                            {{ Form::open(array('url' => 'users/reactive/' . $user->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'POST') }}
                                            {{ Form::submit('Reactive', array('class' => 'btn btn-info')) }}
                                            {{ Form::close() }}

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <p>Your Suspension List Is empty </p>
                        @endif
                        {!! $suspend->render() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
