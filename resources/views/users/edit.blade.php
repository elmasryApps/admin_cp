@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit {{$user->name}}</div>
                    <div class="panel-body">
                        <!-- if there are creation errors, they will show here -->
                        {{ Html::ul($errors->all()) }}


                        {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('password', 'Password') }}
                            {{ Form::text('password', null, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            <label for="password-confirm" class="">Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                        </div>
                        <div class="form-group">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::email('email', null, array('class' => 'form-control')) }}
                        </div>



                        {{ Form::submit('Edit the User!', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
