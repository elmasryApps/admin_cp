@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Offer {{$offer->id}}</div>

                    <div class="panel-body">
                        <p> Text : {{$offer->text}}</p>
                        <p>Url : {{$offer->link}}</p>
                        <hr/>
                        <img src="{{URL::to('/')}}/photo/{{$offer->photo}}" class="img-rounded" width="100%">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
