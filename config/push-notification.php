<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'development',
        'certificate' =>public_path().'/FarmTreeCertificatesDevPush.pem',
        'passPhrase'  =>'123456789',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'yourAPIKey',
        'service'     =>'gcm'
    )

);