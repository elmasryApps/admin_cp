<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Android extends Model
{
    protected $table = 'android';

    protected $fillable = [
        'token', 'user', 'status', 'created_at'
    ];
}
