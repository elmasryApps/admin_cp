<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{

    protected $table = 'notify';

    protected $fillable = [
        'id', 'offer_id', 'updated_at', 'created_at'
    ];

    // this static fanction  will save a notification
    static function Notify($id)
    {
        return Notify::feed($id);
    }

    static public function feed($id)
    {
        //save a new  notification
        $notify = new  Notify([
            'offer_id' => $id
        ]);
        if ($notify->save())
            return true;
        return false;

    }

    // this will return the offer array
    public function offer_array()
    {

        return Offer::where('id', $this->offer_id)->first();
    }

    static function offer_text($id)
    {

        return Offer::where('id', $id)->first();
    }
}
