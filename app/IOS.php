<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IOS extends Model
{
    protected $table = 'ios';
    protected $fillable = [
        'token', 'user', 'status', 'created_at'
    ];


}
