<?php

namespace App;

use Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        'photo', 'text', 'link', 'created_at'
    ];

    public function logs()
    {
        $offer = DB::table('notify')->where('offer_id', $this->id)->get();
        return $offer;
    }
}
