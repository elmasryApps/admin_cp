<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // I hard coded the admin email  to be the only one  who can access the website
        if ($request->user()->email != env('ADMIN_MAIL', 'NOTHING')) {
            \Session::flash('error', 'Not Allowed !');

            return redirect('home');
        }

        return $next($request);

    }
}
