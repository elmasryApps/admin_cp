<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;

class UserController extends Controller
{

    /*
  * Create a new controller instance.
  *
  * @return void
  */
    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::paginate(15);

        // list all Suspended users
        $suspend  = User::onlyTrashed()
            ->paginate(15);
        return view('users.index', ['suspend' => $suspend,'users'=>$users]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput($request->except('password'));
        } else {
            // create user
            $user = new User([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
            ]);
            $user->save();
            // redirect
            Session::flash('status', 'Successfully created User!');
            return Redirect::to('users');
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        // get the user
        $user = User::find($id);

        // show the edit form and pass the user
        return view('users.edit')
            ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $rules = array(
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput($request->except('password'));
        } else {
            // store
            $user = User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = $request->input('password');
            $user->save();

            Session::flash('status', 'Successfully Edited User!');
            return Redirect::to('users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {

        // delete
        $User = User::find($id);
        $User->delete();

        // redirect
        Session::flash('status', 'Successfully Suspended  the User!');
        return Redirect::to('users');
    }

    public function suspended()
    {
        // list all Suspended users
        $users = User::onlyTrashed()
            ->paginate(15);
        return view('users.suspend', ['users' => $users]);
    }

    public function reactive($id)
    {

        if (User::withTrashed()
            ->where('id', $id)
            ->restore()) {
            Session::flash('status', 'Successfully Activated the User!');
            return Redirect::to('users');

        }
        Session::flash('error', 'Error Activate the User!');
        return Redirect::to('users/suspended');
    }
}
