<?php

namespace App\Http\Controllers;

use App\Notify;
use App\Offer;
use Auth;
use Illuminate\Http\Request;


class OfferController extends Controller
{

    /*
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware(['auth','admin'],['except' => ['index']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::guest()){

            // get all the offers
            $offers = Offer::orderBy('created_at', 'desc')->paginate(15);
            return view('offer.index', ['offers' => $offers]);
        }
        return redirect('login');
    }

    public function create()
    {
        return view('offer.create');
    }

    public function view($id)
    {
        $offer = Offer::find($id);
        return view('offer.view', ['offer' => $offer]);
    }

    public function post(Request $request)
    {
        // move the photo
        $file_name = $request->file('photo')->getClientOriginalName();
        $request->file('photo')->move('photo/', $file_name);
        // save the offer
        $offer = new  Offer([
            'text' => $request->input('text'),
            'link' => $request->input('link'),
            'photo' => $file_name
        ]);

        if ($offer->save()) {
            // save a notification log
            Notify::Notify($offer->id);
            // redirect to offer index
            return redirect('offer')->with('status', 'Offer Created!');
        } else {
            // redirect back with error
            return back()->withInput();
        }

    }
}
