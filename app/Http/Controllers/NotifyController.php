<?php

namespace App\Http\Controllers;

use App\Notify;
use Illuminate\Http\Request;

class NotifyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['Log']]);
    }

    // show the notify log
    public function Log()
    {
        $Log = Notify::orderBy('created_at', 'desc')->paginate(15);

        return view('log', ['log' => $Log]);

    }
}
