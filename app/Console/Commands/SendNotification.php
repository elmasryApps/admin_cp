<?php

namespace App\Console\Commands;

use App\IOS;
use Davibennun\LaravelPushNotification\PushNotification;
use Illuminate\Console\Command;

class SendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sending notify to all the users ..';

    protected $ios = null;
    protected $Notify = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IOS $IOS, \App\Notify $Notify)
    {
        parent::__construct();
        // get all the devices token  ios
        $this->ios = $IOS->all();
        // get all the  offers which
        $this->offers = $Notify->where('status', '0')->get();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // for every offer we will send the msg to all the users
        foreach ($this->offers as $offer) {
            ;
            $message = \PushNotification::Message('Message Text', array(
                'badge' => 1,
                'sound' => 'example.aiff',
                'custom' => array('data' => array(
                    'type' => 'offer',
                    'link' => $offer->offer_array()->link,
                    'text' => $offer->offer_array()->text,
                    'photo' => env('APP_URL') . '/public/photo/' . $offer->offer_array()->photo
                ))
            ));
            //   dd($offer->offer_array()->text);
            // send to the all of IOS users
            foreach ($this->ios as $ios) {
                  $re = \PushNotification::app('appNameIOS')
                     ->to($ios->token)
                    ->send($message);

                $offer->status = '1';
                $offer->save();
            }
        }

    }
}
